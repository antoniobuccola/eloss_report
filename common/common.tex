\chapter{Digital Signal Processing}

\section{Extraction of the amplitude of the physical signals}

\begin{enumerate}
\item FPN correction of each signal from each AGET, average of the four FPN signals is used~\cite{fpn}, see Fig.~\ref{fpnComp}.
\item As stated in ref.~\cite{fpn}, the FPN-corrected signal requires a further baseline correction to compensate residual systematic distorsions of the signal. Referring to Fig.~\ref{linComp}, a linear fit of the samples of the signals is performed (excluding, of course, the samples in which the signal itself is present) and then the obtained line is subtracted sample-by-sample from the FPN corrected signal.
\end{enumerate}

\begin{figure}[h!]
	\centering
	\subfloat[][Baseline of a physical signal (in black) from an AGET and FPN signals from the same chip. 
	Average FPN signal is shown in red.]
	{\includegraphics[width=12cm, height=6cm]{common/images/signals_zoom_avg}
	\label{fpnComp}}\ \ \ \
	\subfloat[][Baseline correction using linear fit algorithm.\\ Samples inside the green lines are excluded.]
	{\includegraphics[width=12cm, height=6cm]{common/images/signal}
	\label{linComp}} 
	\caption{Digital signal processing of a typical physical signal.}
	\label{comp}
\end{figure}

\section{Pulser equalization of the amplitude of the physical signals}

Determination of a corrective factor $f_{xy}$, with respect to an arbitrarily chosen reference pad, for each pixel of the pad plane to 
calibrate the gain fluctuations due to the electronic chain of the physical signals. For this purpouse, an external pulser of different amplitudes is sent to the mesh of the micromegas~\cite{ats}. Baseline subtraction is performed using the linear fit algorithm previously described, since FPN channels do not sample correctly the common noise for pulser measurements.

\begin{figure}[h!]
	\centering
	\subfloat[][Example of pulser spectrum.]
	{\includegraphics[width=12cm, height=6cm]{common/images/pulserSpectrum}
	\label{pulserSpectrum}}\ \ \ \
	\subfloat[][Linear fit of the centroids and residuals.]
	{\includegraphics[width=12cm, height=6cm]{common/images/pulserSpectrumFit}
	\label{pulserSpectrumFit}} 
 	\caption{Pulser equalization.}
	\label{equalization}
\end{figure}

For each pixel of the pad plane the spectrum of the pulser amplitude is built (see Fig.~\ref{pulserSpectrum}) and centroids are extracted. A linear fit of the centroids is performed with
$$F = G_{xy}\cdot C + Q_{xy}$$
where $F$ is the value of the centroid normalized to the maximum amplitude and $C$ is the value of the centroid (see Fig.~\ref{pulserSpectrumFit}). The corrective factor is given by $f_{xy} = G_{xy}/G_{00}$, where $G_{00}$ is the corresponding value of $G_{xy}$ for the reference pad.

\chapter{Data Analysis}


\section{Event Selection}

For each beam employed during the experiment $E_{beam}\lesssim 3$ AMeV; thus, the probability of nuclear interaction is very low. Events corresponding to elastic or inelastic scattering are also possible. They have been, however, excluded in this analysis.

\begin{figure}[h!]
	\centering
	\subfloat[][]
	{\includegraphics[width=8cm, height=4cm]{common/images/strange_1}
		\label{strano1}} 
	\hspace{16pt}
	\subfloat[][]
	{\includegraphics[width=8cm, height=4cm]{common/images/strange_2}
		\label{strano2}} \\
	\subfloat[][]
	{\includegraphics[width=8cm, height=4cm]{common/images/strange_3}
		\label{strano3}} 
		\subfloat[][]
	{\includegraphics[width=8cm, height=4cm]{common/images/track}
		\label{traccia}} 
	\caption{Events rejected by the selection (\ref{strano1}, \ref{strano2}, \ref{strano3}) 
	and event corresponding to a ion penetrating into the gas and stopping inside the active volume (\ref{traccia}).}
\end{figure}

Figures~\ref{strano1},~\ref{strano2},~\ref{strano3} show examples of events rejected in this analysis: they do not correspond to a ion penetrating into the gas and then stopping inside the active volume or they correspond to events in which noise fluctuations were acquired because thresholds were overcome; fig.~\ref{traccia} shows a $^6\mathrm{Li}$ ion with energy $E = 9$ MeV travelling inside $\mathrm{CF}_4$ at pressure $P = 69.6$ mbar.

Event selection is based on intrinsic properties of each track:
\begin{itemize}
 \item the total charge collected by the pad plane, obtained by integration of the profile of the single ion, as the one shown, for example, in Fig.~\ref{profile} which is proportional to the sum of the amplitudes of the signals produced by the pads affected by the track of the ion; in this way events in which excitation of the projectile occurred can be rejected
 \item the total number of involved pads and the range of the ion, defined as the distance at which the energy loss profile of the single ion is the 50\% of its own maximum (see Fig.~\ref{profile}), in order to select events whose track has constant width and length.
\end{itemize}


\begin{figure}[h!]
\centering
\includegraphics[scale=0.15]{common/images/profile}
\caption{Energy loss profile of a single ion obtained with a projection onto the $x$-axis of a track as the one represented in Fig.~\ref{traccia}.}
\label{profile}
\end{figure}

\textbf{Selection criteria}: total charge, number of pads and range distributions are built and a gaussian fit is performed; an event is accepted if all the three variables are within two standard deviations from the centroid of the corresponding distributions.

\begin{figure}[h!]
	\centering
	\subfloat[][Total charge distribution]
	{\includegraphics[width=8cm, height=4cm]{common/images/chargeSelec}
		\label{caricaDistrib}} 
	\hspace{16pt}
	\subfloat[][Range distribution]
	{\includegraphics[width=8cm, height=4cm]{common/images/rangeSelec}
		\label{rangeDistrib}} \\
	\subfloat[][Number of pads distributions]
	{\includegraphics[width=8cm, height=4cm]{common/images/npadsSelec}
		\label{npadsDistrib}} 
	\hspace{16pt}
	\subfloat[][Average energy loss profile after event selection. Error bars are smaller than the marker size.]
	{\includegraphics[width=7.5cm, height=4cm]{common/images/bragg_no_calib}
		\label{profiloMedio}} \\
	\caption{}
	\label{selezione}
\end{figure}

Each point of the average profile has been assigned using the following procedure: for every value of $x_i$ the distribution of the energy loss values corresponding to $x_i$ of the selected events has been built (see, for example, Fig.~\ref{distrib50}). From the gaussian fit of the distribution centroid $\hat x$ and standard deviation $\sigma $ are extracted. The value of the point is, then, $\hat x$ and the corresponding error is $\sigma/\sqrt{n}$, where $n$ is the number entries of the distribution, i.e. the number of ions which travelled a distance at least equal to $x_i$.

 \begin{figure}[h!]
 	\centering
 	\includegraphics[scale=0.18]{common/images/dQdx_x_50_mm}
 	\caption{Energy loss distribution (in arbitrary units) at penetration depth $ x_i=57.2 $ mm.}
 	\label{distrib50}
 \end{figure}
 

\section{Check of the stability of the system during a measurement}\label{checkStability}

For each measurement a check of the stability of the system has been performed. The quantities used for this check are the total charge collected by the pad plane and the range, defined in the previous section. Event selection has not been applied of ths check. In order to give a quantitative estimation of the stability, this procedure has been followed. 

Let $X$ be either the total charge or the range of an event and let $\hat X$ and $\sigma_X$ be the centroid and the sigma, respectively, of the distribution of $X$, retrieved with a gaussian fit of the distribution itself. 

The distribution of the quantity

\begin{equation}
 \delta X = \cdot \frac{X-\hat X}{\hat X}
 \label{deltaX}
\end{equation}

has been build. Two smaller samples of the events ($\sim 10\%$ of the total number of acquired events on a single measurement) at the beginning and at the end of the measurement have been selected. A gaussian fit has been performed on the two obtained distributions and their centroids have been retrieved, $\delta X_f$ and $\delta X_l$ respectively. The variation of $X$ along the measurement is defined as

\begin{equation}
  v(X) \equiv \delta X_l - \delta X_f
  \label{variationDef}
\end{equation}

The quantity $v(X)$ must then be compared to the experimental resolution $R(X)$ on $X$:

$$R(X) = \frac{FWHM(X)}{\hat X} = 2\sqrt{2\ln 2}\ \frac{\sigma_X}{\hat X}$$

If $|v(X)| \ll R(X)$ for total charge and range both, the effect on instabilities of the system can be neglected.


\section{Simulation of the energy loss profile}

In order to compare energy loss models and data simulation of the energy loss profile is required. In the analysis for this report two energy loss codes have been employed.

The first code is TRIM (TRansport of Ion in Matter) \cite{srim} with which 10000 events for each pair ion/gas at a given pressure have been simulated. Dead layers (Mylar window inside the ACTAR - beam line coupilng flange, 50 mm of inactive gas between this window and the pad plane) have been taken into account in the simulation. An example of the profile obtained using TRIM is shown in Fig.~\ref{SRIMprofile}.

\begin{figure}[h!]
\centering
\includegraphics[width=10cm, height=5cm]{common/images/bragg_SRIM}
\caption{Energy loss profile simulated with TRIM. Ion: $^6\mathrm{Li}$, $E=9$ MeV. Gas: $\mathrm{CF_4}$, $P=69.6$ mbar.}
\label{SRIMprofile}
\end{figure}

The second code is LISE++ \cite{lise}, which computes the energy loss profile as a function of the kinetic energy of the ion $S(E)$, as shown, for example in Fig.~\ref{LISEprofile_E}.  

\begin{figure}[h!]
\centering
\includegraphics[width=10cm, height=5cm]{common/images/bragg_LISE_E}
\caption{Energy loss profile computed by LISE++. Ion: $^6\mathrm{Li}$, $E=9$ MeV. Gas: $\mathrm{CF_4}$, $P=69.6$ mbar.}
\label{LISEprofile_E}
\end{figure}

In order to retrieve the energy loss profile as a function of the distance travelled by the ion $S(x)$, the continuous slowing down approximation (CSDA) was applied. The general formula of the travelled distance $x$ in CSDA for an ion passing from initial energy $E_i$ to final energy $E_f<E_i$ is

\begin{equation}
 x = x(E_i \rightarrow E_f) = \int_{E_f}^{E_i}\frac{dE}{S(E)}
 \label{csda}
\end{equation}

Let the initial kinetic energy of the ion inside the gas be $E_0$, the energy lost by the ion in a single step $\delta E$ and $S(E)$ its energy loss profile. The complete algorithm is thus described by the following expression:

\begin{equation}
\left\{
\begin{aligned}
&x_0 = 0\\
&x_k = x_{k-1} + \delta x_k\\
&\mathrm{where}\ \delta x_k = \int_{E_{k+1}}^{E_k}\frac{dE}{S(E)}\\
&E_k = E_0 - k\ \delta E\\
&\mathrm{with\ the\ condition}\ E_k> 1~\mathrm{keV/u}\\
&S(x_k) = \frac{\delta E}{\delta x_k} 
\end{aligned}
\right.
\end{equation}

Figure~\ref{LISEprofile_X} shows the result for the profile plotted in fig.~\ref{LISEprofile_E} after this procedure.

\begin{figure}[h!]
\centering
\includegraphics[width=10cm, height=5cm]{common/images/bragg_LISE_X}
\caption{Energy loss profile as a function of distance obtained from LISE++. Ion: $^6\mathrm{Li}$, $E=9$ MeV. Gas: $\mathrm{CF_4}$, $P=69.6$ mbar.}
\label{LISEprofile_X}
\end{figure}

The smooth transition of $S(x)$ to zero is due to fluctuation in the energy lost by the single ion of a monochromatic beam, as shown in ref.~\cite{knoll}, pp. 32 et seq. Since such fluctuations are neglected in CSDA, this smooth transition cannot be observed in the profile obtained by LISE, as also shown, for example, in  Fig.~\ref{LISEprofile_X}.

%%%%%%

\section{Effect of the thermal diffusion}\label{diffSec}

The codes employed for the simulation of the energy loss profile do not account for the thermal diffusion of the ionization electrons. 
As a first step, in order to show the increase of the width of the acquired tracks due to thermal diffusion, a crude estimation in a particular case will be computed. 

The particular case of $\mathrm{CF_4}$ at $P = 69.6$ mbar and $T = 20~ ^\circ\mathrm{C}$ will be presented. Drift field $E_{drift} = 100$ V/cm. The corresponding MAGBOLTZ results are the drift velocity $v_{drift} = 1.13~\mathrm{\mu m/ns}$ and the transverse (i.e. orthogonal to the drift field) diffusion coefficient. Let $h = 12$ cm, distance among the beam and the pad plane. The drift time $t_D$ is estimted as

$$t_{drift} = \frac{h}{v_{drift}} = 1.1~\mathrm{\mu s}$$

The width of the electron distribution after the drift (namely, at the pad plane level) is, then, given by

$$\sigma_{diff} = \sqrt{2D_T~t_{drift}} = 1.03~\mathrm{mm}$$

In Fig.~\ref{img_trackZoom}, a zoom on a particular track of $^6\mathrm{Li}$ ion on $\mathrm{CF_4}$ at the stated value of pressure and temperature is shown. Recalling that the pads are $2\times 2~\mathrm{mm^2}$ each, tracks 3 pad rows wide are expected in this case.

\begin{figure}[h!]
	\centering
	\includegraphics[scale=0.5]{common/images/trackZoom}
	\caption{}
	\label{img_trackZoom}
\end{figure}

%\noindent
%The outlined procedure has been employed to calculate the value of $\sigma_{diff}$ for every analyzed case and the %compatibility among $\sigma_{diff}$ and the data has been checked.

\medskip 
It will now be described the dedicated procedure used to take into account the thermal diffusion in a simulated profile. The energy, that is the integral, of the simulated profile must not be modified, while its shape will, namely it is expected that its maximum to lower and its end-point to increase. 
Each code provides a simulated energy loss profile as a collection of points $\{(x_k, S_k)\}$, where $S_k$ is the stopping power at $x = x_k$. A way to write $S_k$ as a function of the energy lost $E_k$ is

\begin{equation}
	S_k = E_k\delta(x-x_k)
	\label{eq_s_write}
\end{equation}

where $\delta(x)$ is the Dirac delta (generalized) function. It is well known that it can be represented in a limit form as

\begin{equation}
	\delta(x-x_i) = \lim_{\sigma\to 0} \dfrac{1}{\sigma\sqrt{2\pi}} e^{-\dfrac{(x-x_i)^2}{2\sigma^2}}
	\label{eq_delta_gauss}
\end{equation}

It must be reminded that the diffusion acts on the ionization electrons, not on the energy lost by the ion. However, since the energy lost is proprtional to the number of ionization electrons, expressions (\ref{eq_s_write}) and (\ref{eq_delta_gauss}) can be employed to determine the shape of the simulated profile after the diffusion in the following way: let $\hat E_j$ be the energy collected (proportional to the number of electrons) at point $x=x_j$ after the diffusion; this quantity can be written as

\begin{align}
	\hat E_j & = \sum_{k} \int_{x_j - \Delta x}^{x_j+\Delta x} E_k \dfrac{1}{\sigma\sqrt{2\pi}} e^{-\dfrac{(x-x_k)^2}{2\sigma^2}} dx \nonumber \\
	& = \sum_{k} E_k \int_{x_j - \Delta x}^{x_j+\Delta x} \dfrac{1}{\sigma\sqrt{2\pi}} e^{-\dfrac{(x-x_k)^2}{2\sigma^2}} dx\nonumber\\
	& = \sum_{k} E_k \{NCF(x_j+\Delta x; x_k, \sigma) - NCF(x_j-\Delta x; x_k, \sigma)\}
	\label{eq_e_after_diff}
\end{align}

where $ NCF(x; \mu, \sigma) $ is the cumulative distribution of the nurmalized gaussian function with mean $\mu$ and standard deviation $\sigma$. Finally, the shape of the simulated profile is the collection of points  $\{(x_n, \hat S_n)\}$ such that

\begin{equation}
	\hat S_n = \hat E_n\delta(x-x_n)
	\label{eq_diff_s_write}
\end{equation}

in which $\hat E_n$ has been computed wiht eq.~(\ref{eq_e_after_diff}). Figure~\ref{img_diff_no_diff} shows the result for a TRIM profiel in the particular case of $^6\mathrm{Li}$ @ $E=9$ MeV and $\mathrm{CF_4}$ at $P = 69.6$ mbar and $T = 20~ ^\circ\mathrm{C}$.

\begin{figure}[h!]
	\centering
	\includegraphics[scale=0.5]{common/images/diff_or_no_diff}
	\caption{Comparison among the simulated TRIM profile before and after taking onto account the diffusion ($\sigma=$ 1.03 mm) for $^6\mathrm{Li}$ @ $E=9$ MeV and $\mathrm{CF_4}$ at $P = 69.6$ mbar and $T = 20~ ^\circ\mathrm{C}$.}
	\label{img_diff_no_diff}
\end{figure}

%% ___HERE___

%%%%%%

\section{Energy calibration}\label{calibSec}

The calibration factor is unique for all the pads of the pad plane once equalization of electronics channels has been performed. In this analysis the calibration factor has been retrieved in two different ways.

The first one employs the total integral of the profiles: this is the standard method of calibration, that is, normalizing the integral of the average profile (in arbitrary units) to the integral of the simulated profile (in physical units).

An alternative method to calculate the calibration factor employs a scaling relation between two different measurement involving their calibration factors (computed using the normalization to the integral of the two profiles) and the electron multiplication factors at the micromegas level. Details will be explained in the following section.


\section{Linking the calibration factors to the electron multiplication factors}\label{linkMulti}
\newcommand{\multi}{\mathcal{M}}

The calibration factor $C$ is the conversion factor from the response of a detector $\mathcal{Q}$ following an energy release $\Delta E$ inside the active volume of the detector itself.

\begin{equation}
\Delta E [\mathrm{keV}]= C\cdot \mathcal{Q}[\mathrm{a.u.}]
\label{calibDef}
\end{equation}

In case of the ACTAR - TPC demonstrator, the quantity $\mathcal{Q}$ can be further specified in terms of the number $N_0$ of the produced ion-electron pairs and the electron multiplication factor $\multi $ at the micromegas level:
\begin{equation}
\mathcal{Q} = f\cdot Q = f\cdot \multi\cdot N_0 e \label{arbCharge}
\end{equation}

where $f$ is the gain of the electronic chain. In this analysis, $f$ is assumed to be constant along all the measurements. The expressions~(\ref{calibDef}) and~(\ref{arbCharge}) and the relation $W =\Delta E/N_0$,  with $W$ the average energy needed to produce a ion-electron pair in the gas, assumed constant, can be used to deduce a scaling relation for the calibration factors for two different measurements $a$ and $b$:

\begin{align}
\boxed{\dfrac{C_a}{C_b}} &= \dfrac{\Delta E_a/\mathcal{Q}_a}{\Delta E_b/\mathcal{Q}_b} = \dfrac{\Delta E_a}{\Delta E_b}\cdot \dfrac{\mathcal{Q}_b}{\mathcal{Q}_a} = [\mathrm{using}\ (\ref{arbCharge})] \nonumber\\
&= \dfrac{\Delta E_a}{\Delta E_b}\cdot \dfrac{f_b \cdot \multi_b\cdot N^{(b)}_0 e}{f_a\cdot \multi_a\cdot N^{(a)}_0 e} =\ [f\equiv const.,\ W=\Delta E/N_0\equiv const.]\nonumber\\
&= \boxed{\dfrac{\multi_b}{\multi_a}} \label{relation}
\end{align}

In case of the micromegas, the electron multiplication factor $\multi$~\cite{multiplication} can be simply written as $\multi = e^{\alpha d}$,
where $\alpha$ is the ionization coefficient, which is assumed to be correctly computed by MAGBOLTZ~\cite{magboltz}, once the electric field inside the micromegas $E_\mu$ is given, and $d$ is the thickness of the micromegas. The micromegas employed in the ACTAR - TPC demonstrator are 220 $\mathrm{\mu m}$ thick; assuming the thickness of the micromegas is constant and perfectly known, the electric field is simply given by $E_\mu = V_\mu/d$, with $V_\mu$ the applied voltage. Error propagation is performed to take into account experimental errors on the retrieved calibration factors and uncertainties on $\alpha$ estimated by MAGBOLTZ.
\medskip

Equation (\ref{relation}) can be used to calibrate into physical units: given the gas, suppose the calibration factor of a ion $C_{ref}$ is taken as a reference and its electron multiplication factor is $\multi_{ref}$; the calibration factor $C( \mathrm{^AX})$ of another ion in the same gas, whose multiplication factor is is $\multi( \mathrm{^AX})$, is simply given by
\begin{equation}
C( \mathrm{^AX}) = C_{ref}\cdot\dfrac{\multi_{ref}}{\multi( \mathrm{^AX})}
\label{newCalib}
\end{equation}

Once again, propagation has been applied to compute the error associated to the calibration factor. For this check, the calibration factor has been obtained using the value of $\Delta E$ computed by TRIM

\section{Evaluation of the agreement among experimental and simulated profile}

The agreement between the experimental average energy loss profile after calibration $S_{ATS}$ and the simulated profile $S_{sim}$ has been evaluated calculating the parameter
\begin{equation}
Q_F = \frac{1}{N}\sum_{i=1}^{N}\Bigg[\frac{S_{ATS}(x_i) - S_{sim}(x_i)}{\sigma_{ATS}(x_i)}\Bigg]^2
\label{quality}
\end{equation}
where $x_i$ is the penetration depth and $N$ is the number of non-zero points of $S_{ATS}$. The value of $\sigma_{ATS}$ takes into account error propagation of the error on calibration factor and the error on the single point of the profile.
