\select@language {english}
\contentsline {chapter}{\numberline {1}Digital Signal Processing}{3}
\contentsline {section}{\numberline {1.1}Extraction of the amplitude of the physical signals}{3}
\contentsline {section}{\numberline {1.2}Pulser equalization of the amplitude of the physical signals}{4}
\contentsline {chapter}{\numberline {2}Data Analysis}{5}
\contentsline {section}{\numberline {2.1}Event Selection}{5}
\contentsline {section}{\numberline {2.2}Check of the stability of the system during a measurement}{6}
\contentsline {section}{\numberline {2.3}Simulation of the energy loss profile}{7}
\contentsline {section}{\numberline {2.4}Effect of the thermal diffusion}{9}
\contentsline {section}{\numberline {2.5}Energy calibration}{10}
\contentsline {section}{\numberline {2.6}Linking the calibration factors to the electron multiplication factors}{10}
\contentsline {section}{\numberline {2.7}Evaluation of the agreement among experimental and simulated profile}{11}
\contentsline {chapter}{\numberline {3}Results for $\bf {\mathrm {CF_4}}$}{12}
\contentsline {chapter}{\numberline {4}Results for $\bf {\mathrm {iC_4H_{10}}}$}{21}
\contentsline {chapter}{\numberline {5}Results for $\bf {\mathrm {Ar/CH_4}\ (90/10)}$ mixture}{28}
\contentsline {chapter}{Summary}{35}
