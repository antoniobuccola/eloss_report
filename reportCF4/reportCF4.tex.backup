
\chapter{Results for $\bf{\mathrm{CF_4}}$}\label{chapCF4}

Table \ref{chargeStateCF4} shows the ion beams whose energy loss in $\mathrm{iC_4H_{10}}$ has been analyzed. The quantity $ Q $ is the charge state of the beam measured during the experiment.

\begin{table}[h!]
\centering
\begin{tabular}{|l|c|c|c|c|c|c|}
\hline
Ion     &  &  &  &  &  &  \\ \hline
E (MeV) &  &  &  &  &  &  \\ \hline
$Q$     &  &  &  &  &  &  \\ \hline
\end{tabular}
\end{table}

Table \ref{stabilityCF4} summarizes the results for the check outlined in section \ref{checkStability}.

\begin{table}[h!]
\centering
\begin{tabular}{|c|c|c|c|c|c|c|}
\hline
Ion & $E$ (MeV)& $P$ (mbar) & $v(\mathrm{charge})$ & $R(\mathrm{charge})$ (\%) & $v(\mathrm{range})$ & $R(\mathrm{range}) $ (\%)\\ \hline

$ ^{6}\mathrm{Li}$  &9&69.6& 0.18  & 4.1 & 0.09   & 5.2  \\ \hline

$ ^{12}\mathrm{C}$  &36&140& -1.9  & 3.8 & 0.13   & 3.4    \\ \hline

$ ^{14}\mathrm{N}$  &42&141& -0.42 & 3.3 & -0.12  & 3.5   \\ \hline

$ ^{16}\mathrm{O}$  &45&107.4& 0.23  & 2.9 & -0.18  & 3.3   \\ \hline

$ ^{27}\mathrm{Al}$ &75&101& -1.2  & 4.1 & -0.005 & 5.3   \\ \hline

$ ^{50}\mathrm{Ti}$ &142&90& -0.08 & 4.4 & 0.04   & 3.5  \\ \hline
\end{tabular}
\caption{Check of the stability of the system for the ions in $\mathrm{CF_4}$.}
\label{stabilityCF4}
\end{table}

Table \ref{calibComparisonCF4} shows the comparison among the calibration factor obtained using the energy loss calculated with TRIM and the one obtained using LISE. A general good agreement can be observed.

\begin{table}[h!]
\centering
\begin{tabular}{|c|c|c|c|c|c|c|}
\hline
Ion & $E$ (MeV)& $P$ (mbar) & $C$ (TRIM, $\cdot 10^{-2}$ keV/ch) & $C$ (LISE, $\cdot 10^{-2}$ keV/ch) \\ \hline

$ ^{6}\mathrm{Li}$  & 9   & 69.6  & $5.29\pm 0.09$ & $5.12\pm 0.09$ \\ \hline

$ ^{12}\mathrm{C}$  & 36  & 140   & $17.6\pm 0.3$  & $17.7\pm0.3$ \\ \hline

$ ^{14}\mathrm{N}$  & 42  & 141   & $18.3\pm 0.3$  & $19.0\pm0.3$ \\ \hline

$ ^{16}\mathrm{O}$  & 45  & 107.4 & $29.6\pm0.4$   & $30.0\pm0.4$ \\ \hline

$ ^{27}\mathrm{Al}$ & 75  & 101   & $36.4\pm0.6$   & $37.1\pm0.6$ \\ \hline

$ ^{50}\mathrm{Ti}$ & 142 & 90    & $31.8\pm0.5$   & $31.7\pm0.5$ \\ \hline
\end{tabular}
\caption{Comparison of the calibration factor computed using the value of energy obtained from TRIM and LISE for the ions in $\mathrm{CF_4}$.}
\label{calibComparisonCF4}
\end{table}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Table \ref{magboltzCF4} shows the results obtained wiht MAGBOLTZ calculation.

\begin{table}[h!]
\centering
\begin{tabular}{|c|c|c|c|c|}
\hline
Ion & P (mbar) & $V_\mu$ (V)  & $\alpha_T$ $(\mathrm{cm^{-1}})$ & $\alpha_{ATT}$ $(\mathrm{cm^{-1}})$\\ \hline

$ ^{6}\mathrm{Li}$  & 69.6  & 300 & $268.5\pm1.0$ & $0.90\pm0.01$   \\ \hline

$ ^{12}\mathrm{C}$  & 140   & 300 & $217.3\pm0.4$ & $4.40\pm0.03$    \\ \hline

$ ^{14}\mathrm{N}$  & 141   & 300 & $217.0\pm1.5$ & $4.50\pm0.04$   \\ \hline

$ ^{16}\mathrm{O}$  & 107.4 & 250 & $189.4\pm1.5$ & $3.00\pm0.03$   \\ \hline

$ ^{22}\mathrm{Al}$ & 101   & 240 & $184.2\pm1.5$ & $2.80\pm0.03$   \\ \hline

$ ^{50}\mathrm{Ti}$ & 90    & 235 & $187\pm2$     & $2.20\pm0.03$   \\ \hline
\end{tabular}
\caption{Parameters and results for $\alpha_T$ $(\mathrm{cm^{-1}})$ and $\alpha_{ATT}$ $(\mathrm{cm^{-1}})$ with MAGBOLTZ calculation.}
\label{magboltzCF4}
\end{table}


 
Table \ref{summaryTableCF4} the check of the relation (\ref{relation}) is reported. The coefficient $\alpha$ is given by $\alpha = \alpha_T - \alpha_{ATT}$, where $\alpha_T$ and $\alpha_{ATT}$ are given in table \ref{magboltzCF4}. To simplify notations, the ratios $r_C = C_a/C_b$ and $ r_\multi = \multi_b/\multi_a$ are defined, so that the relation (\ref{relation}) reduces to $ r_C = r_\multi$. For this check, the case of $^{6}\mathrm{Li}$, $E = 9$ MeV, $P = 69.6$ mbar is taken as a reference, since it is expected that variation of its effective charge while penetrating into the gas has neglectable influence on the energy loss profile with respect to the resolution of the apparatus.
\medskip


\begin{table}[h!]
\centering
\begin{tabular}{|c|c|c|c|c|c|c|c|c|}
\hline
Ion & P (mbar) & $V_\mu$ (V) & $C$ ($\cdot 10^{-2}$ keV/ch) & $Q_F$ (TRIM) & $\alpha$ $(\mathrm{cm^{-1}})$ & $\multi$ & $r_C\ (\cdot 10^{-3})$ & $r_\multi\ (\cdot 10^{-3})$ \\ \hline

$ ^{6}\mathrm{Li}$  &  69.6   & 300 & \cellcolor[HTML]{F8FF00} $5.29\pm 0.09$ & 213  & $267.6\pm1.1$ & \cellcolor[HTML]{F8FF00} $360\pm8$ &   &  \\ \hline

$ ^{12}\mathrm{C}$  &  140    & 300 & $17.6\pm 0.3$  & 175  & $212.9\pm0.4$ & $108\pm1$ & $301\pm10$ & $300\pm9$  \\ \hline

$ ^{14}\mathrm{N}$  &  141    & 300 & $18.3\pm0.3 $  & 371  & $212.5\pm1.5$ & $107\pm4$  & $289\pm10$ & $297\pm18$ \\ \hline

$ ^{16}\mathrm{O}$  &  107.4  & 250 & $29.6\pm0.4 $  & 434 & $186.4\pm1.5$ & $60\pm2$  & $176\pm5$ & $167\pm9$  \\ \hline

$ ^{27}\mathrm{Al}$ &  101    & 240 & $36.4\pm0.4 $  & 108 & $181.4\pm1.5$ & $54\pm2$  & $143\pm5$ & $150\pm9$  \\ \hline

$ ^{50}\mathrm{Ti}$ &  90     & 235 & $31.7\pm0.5 $  & 434 & $185\pm2$     & $59\pm3$  & $167\pm5$ & $161\pm9$ \\ \hline
\end{tabular}
\caption{Check of the relation (\ref{relation}) for $\mathrm{CF_4}$. Reference values used for the calculation are highlighted. Calibration factors are obtained with normalization of the integral of the simulated profile with TRIM and the experimental one.}
\label{summaryTableCF4}
\end{table}
 
Table \ref{energiesCF4} shows a comparison among the energy lost in the active volume of the gas $E_L$ calculated using the profile given by TRIM and the energy calculated integrating the experimental profile after calibration with the scaling relation (\ref{relation}). The result for $^6\mathrm{Li}$ will not be shown since this case has been used as reference for the equation (\ref{newCalib}). 
\medskip

The error on this value is estimated in this way: given an experimental profile, we consider the two profiles whose values at each point are all by excess or by default of the errorbars at each point of the central  value of the given profile, call them $S_-$ and $S_+$ respectively. Supposing the end point of the profile is $\hat X$, we have 
\begin{equation}
E_L = \int_0^{\hat X}S_0(x)dx 
\label{energyLost}
\end{equation}

where $S_0$ is the central profile, i.e. without errobars, and the lower limit of the integral corresponds to the entrance of the pad plane.
In the same way, we compute
\begin{equation}
E^{(-)}_L = \int_0^{\hat X}S_-(x)dx \ \ \ \ \ \ \ \ \ E^{(+)}_L = \int_0^{\hat X}S_+(x)dx
\label{excess_defect}
\end{equation}

Thus, the error on $E_L$ is given by
\begin{equation}
\delta E = \max\Big\{E_L - E^{(-)}_L, E^{(+)}_L - E_L\Big\}
\label{errorEnergy}
\end{equation}

\begin{table}[h!]
\centering
\begin{tabular}{|c|c|c|c|c|c|}
\hline
 Ion & Beam energy (MeV) & Pressure (mbar) & $E_L$ (TRIM, MeV) & $E_L$ (LISE, MeV) & $E_L$ (exp., scaling, MeV) \\ \hline
$^{12}\mathrm{C}$  & 36  & 140   & 23.6 & 23.6 & $23.6\pm1.1$ \\ \hline
$^{14}\mathrm{N}$  & 42  & 141   & 25.3 & 25.6 & $24.0\pm1.8$ \\ \hline
$^{16}\mathrm{O}$  & 45  & 107.4 & 27.8 & 27.5 & $29\pm2$ \\ \hline
$^{27}\mathrm{Al}$ & 75  & 101   & 39.8 & 38.9 & $38\pm3$ \\ \hline
$^{50}\mathrm{Ti}$ & 142 & 90    & 71   & 71.4 & $72\pm6$ \\ \hline
\end{tabular}
\caption{Comparison of the energy calculation for $\mathrm{CF_4}$.}
\label{energiesCF4}
\end{table}

For $^6\mathrm{Li}$, $E= 9$ MeV at $P = 69.6$ mbar only the results of the calibration with normalization to the integral will be shown, since the calibration factor of this measurement, $C = (5.29\pm0.09)\cdot 10^{-2}$ keV/a.u., has been taken as the reference for the calculation of the calibration factor through the scaling relation (\ref{relation}). 
\bigskip

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  
\begin{figure}[h!]
	\centering
	\subfloat[][Profile comparison. Calibration using TRIM: $C = (5.29\pm0.09)\cdot 10^{-2}$ keV/ch..]
	{\includegraphics[width=15cm, height=7.5cm]{reportCF4/images/run144_with_TRIM}
		\label{run144_TRIM}} \\		
	\subfloat[][Profile comparison. Calibration using LISE: $C = (5.12\pm0.09)\cdot 10^{-2}$ keV/ch.]
	{\includegraphics[width=15cm, height=7.5cm]{reportCF4/images/run144_with_LISE}
		\label{run144_LISE}} \\		
	\subfloat[][Equilibrium charge state as a function of depth based on the energy loss computed by LISE and on charge state compilation of ref.~\cite{chargeState}.]
	{\includegraphics[width=15cm, height=7.5cm]{reportCF4/images/run144_gas_only}
		\label{run144_charge_state}} \\			
	\caption{$\bf{ ^{6}\mathrm{Li}}$, $\bf{E = 9} $ MeV, $\mathrm{CF_4}$, $\bf{P=69.6}$ mbar}
	\label{run144}
\end{figure}
 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{figure}[h!]
	\centering
	\subfloat[][Profile comparison. Calibration using TRIM: $C = (17.6\pm0.9)\cdot 10^{-2}$ keV/ch.]
	{\includegraphics[width=10cm, height=5cm]{reportCF4/images/run192_with_TRIM}
		\label{run192_TRIM}} \\		
	\subfloat[][Profile comparison. Calibration using LISE: $C = (17.7\pm0.9)\cdot 10^{-2}$ keV/ch.]
	{\includegraphics[width=10cm, height=5cm]{reportCF4/images/run192_with_LISE}
		\label{run192_LISE}} \\		
		\subfloat[][Profile comparison. Calibration with the scaling relation: $C = (17.6\pm0.9)\cdot 10^{-2}$ keV/ch.]
	{\includegraphics[width=10cm, height=5cm]{reportCF4/images/run192_with_scaling}
		\label{run192_scaling}}\\
	\subfloat[][Equilibrium charge state as a function of depth based on the energy loss computed by LISE and on charge state compilation of ref.~\cite{chargeState}.]
	{\includegraphics[width=10cm, height=5cm]{reportCF4/images/run192_gas_only}
		\label{run192_charge_state}} \\
	\caption{$\bf{ ^{12}\mathrm{C}}$, $\bf{E = 36} $ MeV, $\mathrm{CF_4}$, $\bf{P=140}$ mbar}
	\label{run192}
\end{figure}
 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\begin{figure}[h!]
	\centering	
	\subfloat[][Profile comparison. Calibration using TRIM: $C = (18.3\pm0.3)\cdot 10^{-2}$ keV/ch.]
	{\includegraphics[width=10cm, height=5cm]{reportCF4/images/run257_with_TRIM}
		\label{run257_TRIM}} \\		
	\subfloat[][Profile comparison. Calibration using LISE: $C = (19.0\pm0.3)\cdot 10^{-2}$ keV/ch.]
	{\includegraphics[width=10cm, height=5cm]{reportCF4/images/run257_with_LISE}
		\label{run257_LISE}} \\		
		\subfloat[][Profile comparison. Calibration with the scaling relation: $C = (17.8\pm1.4)\cdot 10^{-2}$ keV/ch.]
	{\includegraphics[width=10cm, height=5cm]{reportCF4/images/run257_with_scaling}
		\label{run257_scaling}}\\		
	\subfloat[][Equilibrium charge state as a function of depth based on the energy loss computed by LISE and on charge state compilation of ref.~\cite{chargeState}.]
	{\includegraphics[width=10cm, height=5cm]{reportCF4/images/run257_gas_only}
		\label{run257_charge_state}} \\
	\caption{$\bf{ ^{14}\mathrm{N}}$, $\bf{E = 42} $ MeV, $\mathrm{CF_4}$, $\bf{P=141}$ mbar}
	\label{run257}
\end{figure}
 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{figure}[h!]
	\centering
	\subfloat[][Profile comparison. Calibration using TRIM: $C = (30.0\pm0.4)\cdot 10^{-2}$ keV/ch.]
	{\includegraphics[width=10cm, height=5cm]{reportCF4/images/run182_with_TRIM}
		\label{run182_TRIM}} \\		
	\subfloat[][Profile comparison. Calibration using LISE: $C = (29.6\pm0.4)\cdot 10^{-2}$ keV/ch.]
	{\includegraphics[width=10cm, height=5cm]{reportCF4/images/run182_with_LISE}
		\label{run182_LISE}} \\		
		\subfloat[][Profile comparison. Calibration with the scaling relation: $C = (35\pm3)\cdot 10^{-2}$ keV/ch.]
	{\includegraphics[width=10cm, height=5cm]{reportCF4/images/run182_with_scaling}
		\label{run182_scaling}}\\		
	\subfloat[][Equilibrium charge state as a function of depth based on the energy loss computed by LISE and on charge state compilation of ref.~\cite{chargeState}.]
	{\includegraphics[width=10cm, height=5cm]{reportCF4/images/run182_gas_only}
	\label{run182_charge_state}}\\
	\caption{$\bf{ ^{16}\mathrm{O}}$, $\bf{E = 45} $ MeV, $\mathrm{CF_4}$, $\bf{P=107.4}$ mbar}
	\label{run182}
\end{figure}
% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{figure}[h!]
	\centering
	\subfloat[][Profile comparison. Calibration using TRIM: $C = (37.1\pm0.6)\cdot 10^{-2}$ keV/ch.]
	{\includegraphics[width=10cm, height=5cm]{reportCF4/images/run262_with_TRIM}
		\label{run262_TRIM}} \\		
	\subfloat[][Profile comparison. Calibration using LISE: $C = (36.4\pm0.6)\cdot 10^{-2}$ keV/ch.]
	{\includegraphics[width=10cm, height=5cm]{reportCF4/images/run262_with_LISE}
		\label{run262_LISE}} \\		
		\subfloat[][Profile comparison. Calibration with the scaling relation: $C = (38\pm3)\cdot 10^{-2}$ keV/ch.]
	{\includegraphics[width=10cm, height=5cm]{reportCF4/images/run262_with_scaling}
		\label{run262_scaling}}\\	
	\subfloat[][Equilibrium charge state as a function of depth based on the energy loss computed by LISE and on charge state compilation of ref.~\cite{chargeState}.]
{\includegraphics[width=10cm, height=5cm]{reportCF4/images/run262_gas_only}
	\label{run262_charge_state}}\\
	\caption{$\bf{ ^{27}\mathrm{Al}}$, $\bf{E = 75} $ MeV, $\mathrm{CF_4}$, $\bf{P=101}$ mbar}
	\label{run262}
\end{figure}
 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{figure}[h!]
	\centering
	\subfloat[][Profile comparison. Calibration using TRIM: $C = (31.7\pm0.5)\cdot 10^{-2}$ keV/ch.]
	{\includegraphics[width=10cm, height=5cm]{reportCF4/images/run284_with_TRIM}
		\label{run284_TRIM}} \\	
	\subfloat[][Profile comparison. Calibration using TRIM: $C = (31.8\pm0.5)\cdot 10^{-2}$ keV/ch.]
	{\includegraphics[width=10cm, height=5cm]{reportCF4/images/run284_with_LISE}
		\label{run284_LISE}} \\		
	\subfloat[][Profile comparison. Calibration with the scaling relation: $C = (33\pm2)\cdot 10^{-2}$ keV/ch.]
	{\includegraphics[width=10cm, height=5cm]{reportCF4/images/run284_with_scaling}
		\label{run284_scaling}}\\
	\subfloat[][Equilibrium charge state as a function of depth based on the energy loss computed by LISE and on charge state compilation of ref.~\cite{chargeState}.]
   {\includegraphics[width=10cm, height=5cm]{reportCF4/images/run284_gas_only}
	\label{run284_charge_state}}\\
	\caption{$\bf{ ^{50}\mathrm{Ti}}$, $\bf{E = 142} $ MeV, $\mathrm{CF_4}$, $\bf{P=90}$ mbar}
	\label{run284}
\end{figure}
 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%